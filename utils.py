# https://hackersandslackers.com/automate-ssh-scp-python-paramiko/
import paramiko
import os
import psycopg2
from config import dbconfig, serverconfig

from osgeo import gdal

def translate(src_filename, dst_filename):
    """ Convert tif files to mbtiles """
    src_ds = gdal.Open( src_filename )

    # Open output format driver, see gdal_translate --formats for list
    # https://gdal.org/drivers/raster/mbtiles.html#creation-options
    options = gdal.TranslateOptions(
        options=[],
        format='MBTiles', 
        creationOptions=['ZLEVEL=1-20'], 
    )

    translated = gdal.Translate(dst_filename, src_ds, options=options)

    src_ds = None
    translated = None

    image = gdal.Open( dst_filename, 1)
    image.BuildOverviews("AVERAGE", [2,4,8,16])

    image = None

    return translated


def upload():
    # SG SERVER CONFIG:
    remote='119.75.24.67'
    username='ml'
    password='aSm12346'
    localDir='D:\\timah\\script\\mosaic\\mbtiles\\2019-05'
    remoteDir='I:\\timah\\2019-05'

    params = serverconfig()
    print(params)   

    # Open a transport
    transport = paramiko.Transport((remote,22))

    # Auth    
    transport.connect(None,username,password)

    # Go!
    sftp = paramiko.SFTPClient.from_transport(transport)

    try:
        sftp.mkdir(remoteDir, mode=511)
    except IOError:
        pass

    # Download
    # sftp.get(remoteDir,localDir)

    # https://stackoverflow.com/questions/4409502/directory-transfers-on-paramiko
    # Upload
    for item in os.listdir(localDir):
        print(item)
        if os.path.isfile(os.path.join(localDir, item)):
            sftp.put(os.path.join(localDir, item), '%s/%s' % (remoteDir, item))

    # sftp.put(localDir,remoteDir)

    # Close
    if sftp: sftp.close()
    if transport: transport.close()

    pass

def connect():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # read connection parameters
        params = dbconfig()
 
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params)
      
        # create a cursor
        cur = conn.cursor()
        
        # execute a statement
        sql = 'SELECT * FROM public.images'
        cur.execute(sql)
        print("The number of parts: ", cur.rowcount)

        # conn.commit()

 
        # display the PostgreSQL database server version
        print('PostgreSQL database version:')
        cur.execute('SELECT version()')
        db_version = cur.fetchone()
        print(db_version)
       
       # close the communication with the PostgreSQL
        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

    finally:
        if conn is not None:
            conn.close()
            print('Database connection closed.')