import click
import os
from utils import *

@click.group()
def cli():
    pass

@cli.command()

@click.argument('input_dir', type=click.Path(exists=True))
@click.argument('output_dir', default='',)

@click.option('--month', prompt='Month', help='Image Cature Month')
@click.option('--year', prompt='Year', help='Image Capture Year')
@click.option('--publish', default=True, help='Publish tiles')

def tile(input_dir, output_dir, publish, month, year):
    """ Batch Tiling and Publishing of PlanetScope Mosaic for Timah Project"""

    # Create out directory
    directory = os.fsencode(input_dir)
    number = len(os.listdir(directory))

    # Open AOI options
    with open('options/AOI.txt') as f:
        aois = f.read().splitlines()
    with open('options/month.txt') as f:
        months = f.read().splitlines()
    with open('options/year.txt') as f:
        years = f.read().splitlines()

    if (str(int(month)) in months and year in years):

        output_dir = os.path.join(output_dir, year+'-'+month.rjust(2,'0'))
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        # Process files in folder
        click.echo('Processing %i files from %s' %(number, input_dir) )
        for file in os.listdir(directory):
            filename = os.fsdecode(file)

            # Check file type
            if filename.endswith(".tif"): 
                stripped = filename.split('.')[0]

                # Check if it is in AOI options
                if(stripped in aois):
                    click.echo('Processing ' + stripped)

                    # Prepare source and destination
                    src_filename = os.path.join(input_dir, filename)
                    outname = stripped + ".mbtiles"
                    dst_filename = os.path.join(output_dir, outname)

                else:
                    click.echo(stripped + ' is not valid')

                # Process files
                translate(src_filename, dst_filename)
                continue

            else:
                click.echo(filename + ' is not a GeoTiff file')
                continue
    
    else:
        click.echo('Invalid Month and Year')

    if(publish):
        upload()
        connect()

# gdal2tiles
# gdal2mbtiles
# gdal_translate + gdaladdo

# gdal_translate geoTiff_raw.tif -b mask out.tif
# gdal_translate out.tif out.mbtiles -of MBTILES
# gdaladdo -r average output.mbtiles 2 4 8 16

# gdal_translate -of mbtiles mymap3.tif mymap.mbt iles
# gdaladdo -r nearest mymap.mbtiles 2 4 8 16


# /path/gdal2mbt mymap3.tif -z 4-12 -w none -o xyz mymap.mbtiles

