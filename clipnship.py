# importing the requests library 
import requests 
  
# defining the api-endpoint  
API_ENDPOINT = "https://api.planet.com/compute/ops/orders/v2/"
  
# your API key here 
API_KEY = "XXXXXXXXXXXXXXXXX"

# sending get request and saving the response as response object 
r = requests.get(url = URL, data = data) 
  
# extracting data in json format 
data = r.json() 
  
  
# extracting latitude, longitude and formatted address  
# of the first matching location 
latitude = data['results'][0]['geometry']['location']['lat'] 
longitude = data['results'][0]['geometry']['location']['lng'] 
formatted_address = data['results'][0]['formatted_address'] 
  
# printing the output 
print("Latitude:%s\nLongitude:%s\nFormatted Address:%s"
      %(latitude, longitude,formatted_address)) 


# https://developers.planet.com/docs/orders/ordering-delivery
req = {
	"name": "December2019",
	"subscription_id": 0,
	"item_ids" : []
	"item_type": "PSScene4Band",
	"product_bundle": "analytic", #https://developers.planet.com/docs/orders/product-bundles-reference/ 
	"archive_filename": "{{name}}_{{order_id}}.zip",
	"aoi": {'geojson'},
	"url": "string",
}