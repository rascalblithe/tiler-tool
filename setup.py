from setuptools import setup

setup(
    name='TimahImageTiler',
    version='1.0',
    py_modules=['tiler'],
    install_requires=[
        'Click',
    ],
    entry_points='''
        [console_scripts]
        tiler=tiler:cli
    ''',
)